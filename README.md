## ERA5

This is a simple python package for downloading [ERA5](https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation) data from the Copernicus Climate Data Store ([CDS](https://confluence.ecmwf.int/display/CKB/Climate+Data+Store+%28CDS%29+documentation)). It uses the Climate Data Store Application Programming Interface ([CDSAPI](https://pypi.org/project/cdsapi/)) with preset requests for variables used as boundary conditions in TUFLOW FV. Variables include:

*   wind - [u-component](https://apps.ecmwf.int/codes/grib/param-db?id=165) and [v-component](https://apps.ecmwf.int/codes/grib/param-db?id=166) of wind vector 10m above surface
*   temperature - [temperature](https://apps.ecmwf.int/codes/grib/param-db?id=167) of air 2m above surface
*   radiation - [short wave](https://apps.ecmwf.int/codes/grib/param-db?id=235035) and [long wave](https://apps.ecmwf.int/codes/grib/param-db?id=235036) downward radiation at surface
*   pressure - [pressure](https://apps.ecmwf.int/codes/grib/param-db?id=151) adjusted to height of mean sea level
*   humidity - [relative humidity](relative_humidity) at atmospheric pressure level (~1 atm)
*   cloud cover - <a href="">fraction of cloud cover</a> at atmospheric pressure level (~1 atm)
*   precipitation - [total precipitation rate](https://apps.ecmwf.int/codes/grib/param-db?id=235055) at surface
*   waves - [mean direction](https://apps.ecmwf.int/codes/grib/param-db?id=140230), [peak period](https://apps.ecmwf.int/codes/grib/param-db?id=140231), [mean period](https://apps.ecmwf.int/codes/grib/param-db?id=140232) and [significant wave height](https://apps.ecmwf.int/codes/grib/param-db?id=140229) of wave spectra

## Install

To install, download https://downloads.tuflow.com/Python_Wheels/era5-0.0.1-py3-none-any.zip.
Then enter the following into the windows command prompt:

	
    pip install %USERPROFILE%\Downloads\era5-0.0.1-py3-none-any.whl

## API Key

To access and download data from the Copernicus Climate Data Store (CDS) the user must have an API key that is linked to a Copernicus account. To get an API key, first visit this [webpage](https://cds.climate.copernicus.eu/user/login) and setup an account. Next, login and go to this [webpage](https://cds.climate.copernicus.eu/api-how-to). Copy the following two lines from the terminal as displayed on the webpage:

    url: https://cds.climate.copernicus.eu/api/v2
    key: {uid}:{api-key}

Save the two lines of text in a file with the path %USERPROFILE%.cdsapirc. The CDSAPI will automatically search for this document on your computer when authenticating the user for downloads.

## Getting Started

## Authors

*   **Jonah Chorley, Mitchell Smith**