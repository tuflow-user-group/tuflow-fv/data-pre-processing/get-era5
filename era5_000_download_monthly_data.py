"""A basic example of downloading a single variable in monthly intervals from the ERA5 data set"""

from era5 import *

variable = 'precipitation'

xLimit = (151.70035843, 155.61365828)
yLimit = (-28.37383795, -26.48330761)
tLimit = ((2011, 1, 1), (2011, 5, 1))

outFolder = r'.\data\raw'

nameTag = 'MORETON_BAY'

download(variable, xLimit, yLimit, tLimit, outFolder, nameTag)

