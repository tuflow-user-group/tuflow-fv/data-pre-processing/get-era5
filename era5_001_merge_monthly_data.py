"""A basic example of merging monthly files of a single ERA5 variable"""

from era5 import *

variable = 'precipitation'

inFolder = r'.\data\raw'
outFolder = r'.\data'

nameTag = 'MORETON_BAY'

merge(variable, inFolder, outFolder, nameTag)