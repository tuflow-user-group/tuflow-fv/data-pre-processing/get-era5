"""A complex example using subprocess to download all variables at once."""

from era5 import *
import subprocess
import time

variables = ['precipitation', 'wind', 'humidity', 'pressure', 'radiation', 'temperature', 'waves', 'cloud_cover']

xLimit = (151.0, 156.0)
yLimit = (-26.5, -22.5)
tLimit = ((2013, 1, 1), (2013, 4, 1))

outFolder = r'.\data\raw'

nameTag = 'CORAL_SEA'

for variable in variables:
    xLimitStr = '"({:.6f}, {:.6f})"'.format(xLimit[0], xLimit[1])
    ylimitStr = '"({:.6f}, {:.6f})"'.format(yLimit[0], yLimit[1])
    tLimitStr = '"({}, {})"'.format(tLimit[0], tLimit[1])

    cmd = ' '.join(['start py -m era5 download', variable, xLimitStr, ylimitStr, tLimitStr, outFolder, nameTag])
    subprocess.call(cmd, shell=True)
    time.sleep(5)