"""A basic example of reading and plotting ERA5 data"""

from era5 import *
from netCDF4 import Dataset
import matplotlib.pyplot as plt

# modelled data file (ERA5 netCDF4 file)
modFile = os.path.abspath(r'.\data\MORETON_BAY_ERA5_PRECIPITATION_20110101_20110430')

# observed data file (example daily precipitation csv)
obsFile = os.path.abspath(r'.\IDCJAC0009_040913_2011\IDCJAC0009_040913_2011_Data.csv')

# coordinates of observed data
ox, oy = (153.03, 27.47)

# read observed daily rainfall data from csv
csv = np.genfromtxt(obsFile, delimiter=',', skip_header=1, dtype=str)  # read csv
csv[csv == ''] = '0'  # set bad data to zero

ot = datenum(csv[:, 2:5].astype(int)) + 24*3600  # get observed time (end of each day)
op = csv[:, 5].astype(float)  # get observed daily precipitation

# convert observed time from EAST to UTC
ot = ot - 10*3600  # offset by 10 hours

# read modelled precipitation data from ERA5 netCDF4 file
nc = Dataset(modFile, 'r')

mx = nc['longitude'][:].data  # modelled longitude
my = nc['latitude'][:].data  # modelled latitude
mt = nc['time'][:].data  # modelled time (hours from 1900/1/1)

ix = np.argmin(np.abs(mx - ox))  # x-index of nearest point to observation
iy = np.argmin(np.abs(my - oy))  # y-index of nearest point to observation

mp = nc['mtpr'][:, iy, ix].data  # modelled precipitation rate at nearest point (mm/s)

# convert model time to python time (seconds from 1970)
mt = mt.astype(float)*3600 + datenum((1900, 1, 1))

# scale modelled precipitation rate to mm/hour
mp = mp * 3600

# get cumulative rainfall data
mcp = np.cumsum(mp)
ocp = np.cumsum(op)

# convert time from timestamp to datetime object, needed for plotting
mt, ot = datetime(mt), datetime(ot)

# plot the data
plt.plot(mt, mcp, 'r', label='Modelled')
plt.plot(ot, ocp, 'b', label='Observed')

# format the plot
plt.xlim(mt[0], mt[-1])
plt.ylim(0, 800)

plt.legend()

plt.ylabel('Cumulative Rainfall (mm)')

plt.xticks(rotation=25)

plt.show()









