"""A basic example of merging monthly files of a single ERA5 variable"""

from era5 import *

variables = ['precipitation', 'wind', 'humidity', 'pressure', 'radiation', 'temperature', 'waves', 'cloud_cover']

inFolder = r'.\data\raw'
outFolder = r'.\data'

nameTag = 'CORAL_SEA'

for variable in variables:
    merge(variable, inFolder, outFolder, nameTag)
